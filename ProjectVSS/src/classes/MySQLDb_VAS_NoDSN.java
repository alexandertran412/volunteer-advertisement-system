package classes;

/*
* File:    MySQLDbReadFleetNoDSN.java
* Purpose: Used by GUI to Move cursor forward and forward
*          statement = conn.createStatement(
*                      ResultSet.TYPE_SCROLL_INSENSITIVE, 
*                      ResultSet.CONCUR_READ_ONLY);
*/



import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;

public class MySQLDb_VAS_NoDSN {

    static Connection conn;
    static Statement statement;
    static ResultSet rs;

    public static void connectDb() {
        conn = GetConnectionNoDSN.GetConnection();
        if (conn != null) {
            System.out.println("Connected Successfully No DSN!");
        } else {
            System.err.println("Cannot connect to database No DSN");
        }
    }

    public static void readDb() {
        try {
//            statement = conn.createStatement();  //can go backward with out DSN
            statement = conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, 
                                             ResultSet.CONCUR_READ_ONLY);
            rs = statement.executeQuery("SELECT * FROM user");

        } catch (SQLException ex) {
            System.err.println("SQLException: " + ex.getMessage());
        }
    }

//    public static void displayData() {
//        System.out.printf("%-12s%-12s%-15s%15s%15s\n", "UserID", "UserName", "Password", "HighScore", "Date");
//        try {
//            while (rs.next()) {
//                int UserID = rs.getInt("UserID");
//                String UserName = rs.getString("UserName");
//                int Password = rs.getInt("Password");
//                int HighScore = rs.getInt("HighScore");
//                Date Date = rs.getDate("Date");
//                System.out.printf("%-12s%-12s%-15s%15d%15d\n", UserID, UserName, Password, HighScore, Date);
//            }
//        } catch (SQLException ex) {
//            System.err.println("SQLException: " + ex.getMessage());
//        }
//        try {
//            statement.close();
//            conn.close();
//        } catch (SQLException ex) {
//            System.err.println("Error on close: " + ex.getMessage());
//        }
//
//    }

    public static void main(String args[]) throws ClassNotFoundException {
        connectDb();
        readDb();
        //displayData();
    }
}
