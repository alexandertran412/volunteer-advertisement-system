package classes;

import java.sql.*;
import javax.swing.JOptionPane;

public class clsDatabase {
    
    private static final String USERID = "root", PASSWORD = "";
    private static final String URL = "jdbc:mysql:///sep_data";
    
    public static Connection connectDatabase() {
        
        Connection conn = null;
        
        try {
            conn = DriverManager.getConnection(URL, USERID, PASSWORD);
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage());
        }
        
        return conn;
        
    }
    
    
    
}
