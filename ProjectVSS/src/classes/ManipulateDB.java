/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package classes;

import java.sql.*;
import java.util.ArrayList;
import classes.*;

/**
 *
 * @author W.Jn
 */
public class ManipulateDB {
    private Statement stmt;
    private Connection conn;
    private ResultSet rset;
    private String queryString;
    private ArrayList<String> scores;
    
    public ManipulateDB() {
        initializeDB();
    }
    
    private void initializeDB() {
        GetConnectionNoDSN.GetConnection();
        
        try {
            Class.forName("com.mysql.jdbc.Driver");
            
            conn = DriverManager.getConnection("jdbc:mysql://localhost/sep_data", "root", "");
            
            stmt = conn.createStatement();
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    public ArrayList<String> getinfo() {
        scores = new ArrayList();
        String info = "";
        scores.clear();
        
        try {
            queryString = "select * from user";
            rset = stmt.executeQuery(queryString);
            
            while(rset.next()) {
            info = rset.getInt(1)+ ",";
            info += rset.getString(2) + ",";
            info += rset.getString(3) + ",";
            info += rset.getString(4) + ",";
            info += rset.getString(5) + ",";
            info += rset.getDate(6) + ",";
            
            scores.add(info);
            }
        }
        catch(Exception ex) {
            ex.printStackTrace();
        }
        
        return scores;
    }
    
//    public void insertScores(int hits, int misses) {
//        try {
//            stmt = conn.createStatement();
//            String insertString = "insert into scores (username, hits, misses) values ('"+LoginAndRegister.username+"', '"+hits+"', '"+misses+"')";
//            stmt.executeUpdate(insertString);
//        }                                    
//        catch(Exception ex) {
//            ex.printStackTrace();
//        }
//    }
}
