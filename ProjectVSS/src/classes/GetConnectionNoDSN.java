package classes;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class GetConnectionNoDSN {
   
  //private static final String DRIVER ="sun.jdbc.odbc.JdbcOdbcDriver"; // With DSN 
  //private static final String URL = "jdbc:odbc:MySQLFleetDSN";        // With DSN 
    private static final String DRIVER ="com.mysql.jdbc.Driver";        // No DSN,
    private static final String URL = "jdbc:mysql:///sep_data";  // No DSN, name of the database as in MySql, and needs the Driver
    
    private static final String USERID ="root", PASSWORD = "";
      
    static Connection conn;

    public static Connection GetConnection() {
        try {
            Class.forName(DRIVER);  // Load ODBC driver
            try {
                conn = DriverManager.getConnection(URL, USERID, PASSWORD);
            } catch (SQLException ex) {
                System.err.println("SQLException: " + ex.getMessage());
            }
        } catch (ClassNotFoundException ex) {
            System.err.println("Load Driver Exception: " + ex.getMessage());
        }

        return conn;
    }
    
    public static void main(String args[]) throws ClassNotFoundException {
        conn = GetConnection();
        if (conn != null) {
           System.out.println("Connected Successfully No DSN!");
        }
        else {
            System.err.println("Cannot connect to database No DSN");
        }
    }
}
