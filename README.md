# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This repository contains the source code for the Volunteering Advertisement System which is a Java based application that advertises various volunteering positions where an applicant can apply for positions.  

### How do I get set up? ###

Download the source code using the git clone command in Terminal on a Unix based operating system or for Windows use this: https://git-scm.com/download/win to get the various git executables.

git clone https://alexandertran412@bitbucket.org/alexandertran412/volunteer-advertisement-system.git

Use Netbeans 8.x to import, edit and compile the code

### Contribution guidelines ###

Send a push request to Andy (andyuts) or Jacob (jacobefendiuts) to review and merge your code. 

### Who do I talk to? ###

